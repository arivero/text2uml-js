# README #

At this time, this is more a "project of a project" , an expression of desire.

### What is this repository for? ###

* The goal is to create a tool that draws UML diagrams from textual descriptions. It should run completely client side, so a javascript platform has been chosen. 

### Who do I talk to? ###

* If you want to collaborate or have any ideas, use the tools provided by bitbucket ;)