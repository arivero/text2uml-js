start = model

model = sep* relationship (sep* relationship)*

relationship = element (link element)*

element = '[' nombre:(absC:abstractClass / inte:interface / c:class) ('|' attributeList)* ']'


abstractClass = nombre:('<<' elementName '>>')
{return "abstract:" + nombre[1]}

interface = nombre:('<' nom2:elementName '>')
{return "interface:" + nombre[1]}

class = nombre:elementName
{return "clase:" + nombre}

elementName = identifier

attributeList = identifier (';' identifier)


link = '-'


identifier = letraValida:([a-z]i/[0-9]/'_'/'-'/' ' )+ {return letraValida.join("")}

sep
  = [\t\r\n,]
